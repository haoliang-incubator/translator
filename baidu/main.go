package main

import (
	"errors"
	"flag"
	"fmt"
	"github.com/BurntSushi/toml"
	"gitlab.com/haoliang-incubator/translator/baidu/api"
	"log"
	"os"
)

// todo
// * adapter layer

var cfg = &struct {
	URI    string `toml:"uri"`
	Appid  int    `toml:"appid"`
	Secret string `toml:"secret"`
}{}

func init() {
	_, err := toml.DecodeFile("translator.baidu.toml", cfg)
	if err != nil {
		log.Panic(err)
	}
}

func main() {
	if empty(cfg.URI) || empty(cfg.Appid) || empty(cfg.Secret) {
		lastworld(errors.New("invalid config: lace of uri, appid or secret"))
	}

	var q string
	if err := parseArg(&q); err != nil {
		lastworld(err)
	}

	c := api.NewClient(cfg.URI, cfg.Appid, cfg.Secret, api.GenSalt())

	r, err := c.Translate(q)

	if err != nil {
		lastworld(err)
	}

	fmt.Println(r)
}

func parseArg(q *string) error {
	flag.StringVar(q, "q", "", "strings need to be translated")

	flag.Parse()

	if empty(*q) {
		return fmt.Errorf("requires argument: -q")
	}

	return nil
}

func parseConfig() error {
	if empty(cfg.URI) || empty(cfg.Appid) || empty(cfg.Secret) {
		return errors.New("invalid config: lace of uri, appid or secret")
	}

	return nil
}

func lastworld(err error) {
	if _, e := fmt.Fprintln(os.Stderr, err.Error()); e != nil {
		log.Fatalln(e)
	}
	os.Exit(1)
}

func empty(i interface{}) bool {
	switch v := i.(type) {
	case string:
		return v == ""
	case int:
		return v == 0
	default:
		panic("unsupported variable type.")
	}
}
