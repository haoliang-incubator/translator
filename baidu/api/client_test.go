package api

import (
	"testing"
)

func TestTranslate(t *testing.T) {

	// todo
	// * mock server
	uri := "https://fanyi-api.baidu.com/api/trans/vip/translate"
	appid := 2015063000000001
	salt := 1435660288
	secret := "12345678"
	q := "apple"
	ret := "苹果"
	c := NewClient(uri, appid, secret, salt)

	if r, err := c.Translate(q); err == nil {
		if r.String() != ret {
			t.Error("un-matched result, got", r.String())
		}
	} else {
		t.Error(err)
	}
}
