package api

import (
	"testing"
)

func TestSign(t *testing.T) {

	appid := 2015063000000001
	salt := 1435660288
	secret := "12345678"
	sign := "f89f9594663708c1605f3d736d01d2d4"
	q := "apple"

	if Sign(appid, secret, salt, q) != sign {
		t.Error("un-matched sign")
	}
}
