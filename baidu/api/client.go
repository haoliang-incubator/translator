package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

// Client see http://api.fanyi.baidu.com/api/trans/product/apidoc
type Client struct {
	uri         string
	appid, salt int
	secret      string
	client      *http.Client
}

// Translate query
func (c Client) Translate(q string) (fmt.Stringer, error) {

	// todo
	// * 错误处理, see http://api.fanyi.baidu.com/api/trans/product/apidoc
	resp, err := c.client.Get(c.buildQueryString(q))
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			log.Panic(err)
		}
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New(resp.Status)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	r, err := parseResult(body)
	if err != nil {
		return nil, err
	}

	if err := r.Validate(); err != nil {
		return nil, err
	}

	return r, nil
}

func (c Client) buildQueryString(q string) string {
	v := url.Values{}
	v.Add("q", q)
	v.Add("from", "auto")
	v.Add("to", "zh")
	v.Add("appid", strconv.Itoa(c.appid))
	v.Add("salt", strconv.Itoa(c.salt))
	v.Add("sign", c.sign(q))

	return fmt.Sprintf("%s?%s", c.uri, v.Encode())
}

func (c Client) sign(q string) string {
	return Sign(c.appid, c.secret, c.salt, q)
}

// NewClient get new client
func NewClient(uri string, appid int, secret string, salt int) *Client {
	c := new(Client)

	c.uri = uri
	c.appid = appid
	c.salt = salt
	c.secret = secret
	c.client = &(http.Client{
		Timeout: 10 * time.Second,
	})

	return c
}

func parseResult(raw []byte) (Result, error) {
	var r Result

	err := json.Unmarshal(raw, &r)

	return r, err
}
