package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

// Client see http://api.fanyi.baidu.com/api/trans/product/apidoc
type Client struct {
	uri    string
	appid  string
	salt   int
	secret string
	client *http.Client
}

// Translate query
func (c Client) Translate(q string) (fmt.Stringer, error) {

	// todo
	// * 错误处理, see http://api.fanyi.baidu.com/api/trans/product/apidoc
	resp, err := c.client.Get(c.buildQueryString(q))
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			panic(err)
		}
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New(resp.Status)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	ret, err := parseResult(body)
	if err != nil {
		return nil, err
	}

	if err := ret.Validate(); err != nil {
		return nil, err
	}

	return ret, nil
}

func (c Client) buildQueryString(q string) string {
	v := url.Values{}
	v.Add("q", q)
	v.Add("appKey", c.appid)
	v.Add("salt", strconv.Itoa(c.salt))
	v.Add("from", "en")
	v.Add("to", "zh-CHS")
	v.Add("sign", c.sign(q))

	return fmt.Sprintf("%s?%s", c.uri, v.Encode())
}

func (c Client) sign(q string) string {
	return Sign(c.appid, c.secret, c.salt, q)
}

// NewClient get new client
func NewClient(uri, appid, secret string, salt int) *Client {
	c := new(Client)

	c.uri = uri
	c.appid = appid
	c.salt = salt
	c.secret = secret
	c.client = &(http.Client{
		Timeout: 10 * time.Second,
	})

	return c
}

func parseResult(raw []byte) (*Result, error) {
	r := new(Result)

	return r, json.Unmarshal(raw, r)
}
