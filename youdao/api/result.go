package api

import (
	"errors"
	"fmt"
	"strings"
)

// Result translated result
type Result struct {
	Q string   `json:"query"`
	T []string `json:"translation"`

	Web   []WebResult `json:"web"`
	Basic BasicResult `json:"basic"`
	AbnormalResult
}

func (r Result) String() string {
	s := []string{}

	s = append(s, fmt.Sprintf("%s: %s", r.Q, r.T))

	if r.Basic.Phonetic != "" || len(r.Basic.Explain) > 0 {
		s = append(s, "\n单词释义：")
		s = append(s, fmt.Sprintf("* 发音: %s", r.Basic.Phonetic))
		for _, explain := range r.Basic.Explain {
			s = append(s, fmt.Sprintf("* %s", explain))
		}
	}

	if len(r.Web) > 0 {
		s = append(s, "\n网络释义：")
		for _, web := range r.Web {
			s = append(s, fmt.Sprintf("* %s: %s", web.Key, web.Value))
		}
	}

	return strings.Join(s, "\n")
}

// WebResult 网络释义
type WebResult struct {
	Key   string   `json:"key"`
	Value []string `json:"value"`
}

// BasicResult 基本结果, {音标,译文}
type BasicResult struct {
	// todo
	// * 音标
	// * 发音

	Phonetic string   `json:"phonetic"`
	Explain  []string `json:"explains"`
}

// AbnormalResult 异常结果
type AbnormalResult struct {
	ErrCode string `json:"errorCode"`
}

var failReason = map[string]string{
	"101": "缺少必填的参数，出现这个情况还可能是et的值和实际加密方式不对应",
	"102": "不支持的语言类型",
	"103": "翻译文本过长",
	"104": "不支持的API类型",
	"105": "不支持的签名类型",
	"106": "不支持的响应类型",
	"107": "不支持的传输加密类型",
	"108": "appKey无效，注册账号， 登录后台创建应用和实例并完成绑定， 可获得应用ID和密钥等信息，其中应用ID就是appKey（ 注意不是应用密钥）",
	"109": "batchLog格式不正确",
	"110": "无相关服务的有效实例",
	"111": "开发者账号无效",
	"113": "q不能为空",
	"201": "解密失败，可能为DES,BASE64,URLDecode的错误",
	"202": "签名检验失败",
	"203": "访问IP地址不在可访问IP列表",
	"205": "创建的应用（Android、iOS、Web）与调用接口不一致",
	"301": "辞典查询失败",
	"302": "翻译查询失败",
	"303": "服务端的其它异常",
	"401": "账户已经欠费",
	"411": "访问频率受限,请稍后访问",
	"412": "长请求过于频繁，请稍后访问",
}

// Validate validate the result if have error
func (r Result) Validate() error {
	if r.ErrCode == "0" {
		return nil
	}

	if reason, ok := failReason[r.ErrCode]; ok {
		return errors.New(reason)
	}

	return fmt.Errorf("unknown remote error, errorCode: %v", r.ErrCode)
}
