package api

import (
	"crypto/md5"
	"fmt"
	"time"
)

// GenSalt generate a random salt
func GenSalt() int {
	return int(time.Now().Unix())
}

// Sign generate signature
func Sign(appid, secret string, salt int, q string) string {
	plain := fmt.Sprintf("%v%v%v%v", appid, q, salt, secret)

	return fmt.Sprintf("%x", md5.Sum([]byte(plain)))
}
