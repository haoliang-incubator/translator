module gitlab.com/haoliang-incubator/translator/youdao

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/spf13/viper v1.7.0
)
